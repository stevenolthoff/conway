import React from "react"
import "./board.css"

enum CellState {
  Empty = 0,
  Alive = 1,
  Dead = 2,
}

export default class Board extends React.Component<any, any> {
  numRows = 50
  numCols = 50
  interval = {}

  constructor(props: React.Props<any>) {
    super(props)
    const board = this.getInitialBoard(this.numRows, this.numCols)
    this.state = {
      board,
    }
  }

  componentDidMount() {
    this.interval = this.startUpdates()
  }

  startUpdates() {
    const interval = setInterval(() => {
      this.updateBoard()
    }, 100)
    return interval
  }

  getRandomCellState(): CellState {
    return Math.floor(Math.random() * 2)
  }

  getInitialBoard(numRows: number, numCols: number): CellState[][] {
    let newBoard = []
    for (let row = 0; row < numRows; row += 1) {
      let newRow = []
      for (let column = 0; column < numCols; column += 1) {
        newRow.push(this.getRandomCellState())
      }
      newBoard.push(newRow)
    }
    return newBoard
  }

  updateBoard() {
    const board = this.state.board
    for (let i = 0; i < this.numRows; i += 1) {
      for (let j = 0; j < this.numCols; j += 1) {
        const numLiveNeighbors = this.getNumLiveNeighbors(i, j)
        const newState = this.getCellUpdate(board[i][j], numLiveNeighbors)
        board[i][j] = newState
      }
    }
    this.setState({ board })
  }

  getNumLiveNeighbors(i: number, j: number) {
    let numLiveNeighbors = 0

    const range = [-1, 0, 1]
    range.forEach((rowAddition) => {
      range.forEach((colAddition) => {
        if (rowAddition === 0 && colAddition === 0) return
        const neighborRow = i + rowAddition
        const neighborCol = j + colAddition
        const rowIsValid = neighborRow >= 0 && neighborRow < this.numRows
        const colIsValid = neighborCol >= 0 && neighborCol < this.numCols
        if (!rowIsValid || !colIsValid) return
        const cell: CellState = this.state.board[neighborRow][neighborCol]
        if (cell === CellState.Alive) numLiveNeighbors += 1
      })
    })

    return numLiveNeighbors
  }

  getCellUpdate(currentState: CellState, numLiveNeighbors: number) {
    if (currentState === CellState.Alive) {
      if (numLiveNeighbors === 2 || numLiveNeighbors === 3) {
        return CellState.Alive
      } else {
        return CellState.Empty
      }
    }
    if (currentState === CellState.Empty) {
      if (numLiveNeighbors === 3) {
        return CellState.Alive
      } else {
        return CellState.Empty
      }
    }
    return CellState.Empty
  }

  render() {
    const board = this.state.board
    const rows = board.map((row: CellState[], rowIndex: number) => {
      return (
        <div className="row" key={`row-${rowIndex}`}>
          {row.map((column: CellState, colIndex: number) => {
            const cellState = board[rowIndex][colIndex]
            const stateClassName =
              cellState === CellState.Alive ? "alive" : "empty"
            return (
              <div
                className={["cell", stateClassName].join(" ")}
                key={`row-${rowIndex}-col-${colIndex}`}
              >
                {column}
              </div>
            )
          })}
        </div>
      )
    })
    return (
      <div className="container">
        <div className="board">{rows}</div>
      </div>
    )
  }
}
